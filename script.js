const todoInput = document.querySelector(".todo-input");
const todoButton = document.querySelector(".todo-button");
const todoList = document.querySelector(".todo-list");
const filterOption = document.querySelector(".filter-todo");

document.addEventListener("DOMContentLoaded", getLocalTodos);
todoButton.addEventListener("click", addTodo);
todoList.addEventListener("click", deleteCheck);
filterOption.addEventListener("change", filterTodo);

function addTodo(event) {
    event.preventDefault();
    const todoDiv = document.createElement("div");
    todoDiv.classList.add("todo");
    const newTodo = document.createElement("li");
    newTodo.innerText = todoInput.value; 
    newTodo.classList.add("todo-item");
    todoDiv.appendChild(newTodo);
    //ADDING TO LOCAL STORAGE
    //saveLocalTodos(todoInput.value);
    postData(todoInput.value);
    
    const completedButton = document.createElement("button");
    completedButton.innerHTML = '<i class="fas fa-check-circle"></li>';
    completedButton.classList.add("complete-btn");
    todoDiv.appendChild(completedButton);

    const trashButton = document.createElement("button");
    trashButton.innerHTML = '<i class="fas fa-trash"></li>';
    trashButton.classList.add("trash-btn");
    todoDiv.appendChild(trashButton);
    
    todoList.appendChild(todoDiv);
    todoInput.value = "";
}

function deleteCheck(e) {
    const item = e.target;

    if(item.classList[0] === "trash-btn") {
        const todo = item.parentElement;
        todo.classList.add("slide");

        removeLocalTodos(todo);
        todo.addEventListener("transitionend", function() {
            todo.remove();
        });
    }

    if(item.classList[0] === "complete-btn") {
        const todo = item.parentElement;
        todo.classList.toggle("completed");
    }
}

function filterTodo(e) {
    const todos = todoList.childNodes;
    todos.forEach(function(todo) {
        switch(e.target.value) {
            case "all": 
                todo.style.display = "flex";
                break;
            case "completed": 
                if(todo.classList.contains("completed")) {
                    todo.style.display = "flex";
                } else {
                    todo.style.display = "none";
                }
                break;
            case "incomplete":
                if(!todo.classList.contains("completed")) {
                    todo.style.display = "flex";
                } else {
                    todo.style.display = "none";
                }
                break;
        }
    });
}

function saveLocalTodos(todo) {
    let todos;
    if(localStorage.getItem("todos") === null) {
        todos = [];
    } else {
        todos = JSON.parse(localStorage.getItem("todos"));
    }
    todos.push(todo);
    localStorage.setItem("todos", JSON.stringify(todos));
}

function getLocalTodos() {
    let todos = [];
    var xhr = new XMLHttpRequest();

      // Set the HTTP method and URL
      xhr.open("GET", "http://localhost:8080/todo");

      // Set the request header to specify the content type
      xhr.setRequestHeader("Content-Type", "application/json");

      // Define a callback function to handle the response
      xhr.onload = function() {
        if (xhr.status === 200) {
          todos = JSON.parse(xhr.responseText);
          // Handle the response data
          console.log(todos);
              todos.forEach(function(todo) {
                  console.log(todo);
                  const todoDiv = document.createElement("div");
                  todoDiv.classList.add("todo");
                  const newTodo = document.createElement("li");
                  newTodo.innerText = todo.title;
                  newTodo.classList.add("todo-item");
                  todoDiv.appendChild(newTodo);

                  const completedButton = document.createElement("button");
                  completedButton.innerHTML = '<i class="fas fa-check-circle"></li>';
                  completedButton.classList.add("complete-btn");
                  todoDiv.appendChild(completedButton);

                  const trashButton = document.createElement("button");
                  trashButton.innerHTML = '<i id="delete-"+todo.id+" class="fas fa-trash"></li>';
                  trashButton.classList.add("trash-btn");
                  todoDiv.appendChild(trashButton);

                  todoList.appendChild(todoDiv);
              });
        }
      };

      // Send the GET request
      xhr.send();


}

function postData(title) {
      // Get the input field value
//      var title = document.querySelector('.title').value;

      // Create a data object
      var data = {
        title: title
      };

      // Create a new XMLHttpRequest object
      var xhr = new XMLHttpRequest();

      // Set the HTTP method, URL, and asynchronous flag
      xhr.open('POST', 'http://localhost:8080/todo', true);

      // Set the request headers
      xhr.setRequestHeader('Content-Type', 'application/json');

      // Set the callback function
      xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
          console.log('Post request successful!');
          // Handle the response here if needed
        }
      };

      // Convert the data object to JSON string
      var jsonData = JSON.stringify(data);

      // Send the POST request
      xhr.send(jsonData);
    }


function removeLocalTodos(todo) {
    let todos;
    if(localStorage.getItem("todos") === null) {
        todos = [];
    } else {
        todos = JSON.parse(localStorage.getItem("todos"));
    }

    const todoIndex = todo.children[0].innerText;
    todos.splice(todos.indexOf(todoIndex), 1);
    localStorage.setItem("todos", JSON.stringify(todos));
}